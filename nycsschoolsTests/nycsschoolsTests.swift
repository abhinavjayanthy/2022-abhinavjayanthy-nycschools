//
//  nycsschoolsTests.swift
//  nycsschoolsTests
//
//  Created by Abhinav Jayanthy on 4/24/22.
//

import XCTest
@testable import nycsschools

class nycsschoolsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSchoolVM() throws {
        let expectation = self.expectation(
            description: "First element")
        let viewModel = SchoolsViewModel()
        
        viewModel.schools.bind { schools in
    
            if let firstSchool = schools.first, firstSchool.id?.uppercased() == "02m260".uppercased(){
                expectation.fulfill()
            }
        }
        
        // sensible time to wait for the response to come back
        waitForExpectations(timeout: 8, handler: nil)
    }
    
    func testCountSchools() throws {
        let expectation = self.expectation(
            description: "Count schools")
        let viewModel = SchoolsViewModel()
        
        viewModel.schools.bind { schools in
            if schools.count == 440 {
                expectation.fulfill()
            }
        }
        
        // sensible time to wait for the response to come back
        waitForExpectations(timeout: 8, handler: nil)
    }
    
    func testCountResults() throws {
        let expectation = self.expectation(
            description: "Count results")
        let viewModel = ShowResultsViewModel()
        
        viewModel.satResult.bind { results in
            if results.count == 478 {
                expectation.fulfill()
            }
        }
        
        // sensible time to wait for the response to come back
        waitForExpectations(timeout: 8, handler: nil)
    }
    
    func testKnownElement() throws {
        let expectation1 = expectation(
            description: "known element")
        let expectation2 = expectation(
            description: "known element")
        let resultsViewModel = ShowResultsViewModel()
        let schoolViewModel = SchoolsViewModel()
        let knownID = "01M292"
        resultsViewModel.satResult.bind { results in
            if  results.contains(where: {$0.id?.uppercased() == knownID.uppercased()
            }){
                expectation1.fulfill()
            }
                
        }
        
        schoolViewModel.schools.bind { schools in
            if schools.contains(where: {$0.id?.uppercased() == knownID.uppercased()
            }){
                expectation2.fulfill()
            }
        }
        
        // sensible time to wait for the response to come back
        waitForExpectations(timeout: 8, handler: nil)
    }

}
