//
//  Box.swift
//  nycsschools
//
//  Created by Abhinav Jayanthy on 4/24/22.
//

import Foundation

final class Box<T>{
    /*
     This class is used for data binding thru the use of Boxing, this uses property observers to notify any observers that the value has changed
     
     */
    typealias Listener = (T) -> Void
    
    var listener: Listener?
    
    var value:T {
        didSet{
            listener?(value)
        }
    }
    
    init(_ value:T){
        self.value = value
    }
    
    func bind(listener:Listener?){
        self.listener = listener
        listener?(value)
    }
}
