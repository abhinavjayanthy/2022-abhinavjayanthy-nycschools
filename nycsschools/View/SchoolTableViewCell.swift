//
//  SchoolTableViewCell.swift
//  nycsschools
//
//  Created by Abhinav Jayanthy on 4/24/22.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    /* Each table view cell to show each of the school info */
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolOverview: UILabel!
    @IBOutlet weak var schoolPhoneNumber: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(withSchool school:School){
        if let name = school.schoolName {
            schoolName.isHidden = false
            schoolName.text = name
        }else{
            schoolName.isHidden = true
        }
        
        if let phoneNumber = school.phoneNumber {
            schoolPhoneNumber.isHidden = false
            schoolPhoneNumber.text = phoneNumber
        }else{
            schoolPhoneNumber.isHidden = true
        }
        
        if let overview = school.overviewParagraph {
            schoolOverview.isHidden = false
            schoolOverview.text = overview
        }else{
            schoolOverview.isHidden = true
        }
    }
    
    override func prepareForReuse() {
        // Since we are using a reusable cell, we have to flush the data out before the cell is reused to avoid showing of repeated data
        
        schoolName.isHidden = false
        schoolName.text = ""
        schoolPhoneNumber.isHidden = false
        schoolName.text = ""
        schoolOverview.isHidden = false
        schoolOverview.text = ""
    }
    
}
