//
//  SATResult.swift
//  nycsschools
//
//  Created by Abhinav Jayanthy on 4/24/22.
//

import Foundation
/*
 SAMPLE JSON :
 {"dbn":"01M292","school_name":"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES","num_of_sat_test_takers":"29","sat_critical_reading_avg_score":"355","sat_math_avg_score":"404","sat_writing_avg_score":"363"}
 */
struct SATResult :Codable {
    let id:String?
    let schoolName:String?
    let numOfSatTestTakers:String?
    let satCriticalReadingAvgScore:String?
    let satMathAvgScore:String?
    let satWritingAvgScore:String?
    
    enum CodingKeys: String, CodingKey {
        case schoolName
        case id = "dbn"
        case numOfSatTestTakers
        case satCriticalReadingAvgScore
        case satMathAvgScore
        case satWritingAvgScore
    }
}
