//
//  MainCoordinator.swift
//  nycsschools
//
//  Created by Abhinav Jayanthy on 4/24/22.
//

import UIKit

class MainCoordinator: Coordinator {
    /* Coordinator for the whole app  */
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    private let showResultsViewModel = ShowResultsViewModel()
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        /* Starts showing the table view */
        let schoolTableViewController = SchoolsTableViewController.instantiate()
        schoolTableViewController.coordinator = self
        navigationController.pushViewController(schoolTableViewController, animated: true)
    }
    
    func showResults(withSelectedSchool school :School){
        /* Shows the results view and also provided the view model */
        let showResultsViewController = ShowResultsViewController.instantiate()
        showResultsViewController.coordinator = self
        showResultsViewController.viewModel = showResultsViewModel
        showResultsViewController.currentSchool = school
        navigationController.pushViewController(showResultsViewController, animated: true)
    }
}
