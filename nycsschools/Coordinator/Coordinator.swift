//
//  Coordinator.swift
//  nycsschools
//
//  Created by Abhinav Jayanthy on 4/24/22.
//

import UIKit

protocol Coordinator {
    /* Blueprint of a coordinator */
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}
