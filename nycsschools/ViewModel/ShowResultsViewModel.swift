//
//  ShowResultsViewModel.swift
//  nycsschools
//
//  Created by Abhinav Jayanthy on 4/24/22.
//

import Foundation


protocol ResultsProtocol {
    func getSATResults()
    var satResult:Box<[SATResult]> { get set }
    var mathScore:Box<String?> { get set }
    var readingScore:Box<String?> { get set }
    var writingScore:Box<String?> { get set }
    var schoolName:Box<String?> { get set }
    func getCurrentSchoolResults(withSchoolName:String?,withSchoolID id: String)
}

class ShowResultsViewModel:ResultsProtocol {
    /* View model powering the results view contoller */
    
    var satResult       = Box<[SATResult]>([])
    var mathScore       = Box<String?>(nil)
    var schoolName      = Box<String?>(nil)
    var readingScore    = Box<String?>(nil)
    var writingScore    = Box<String?>(nil)
    
    init(){
        getSATResults()
    }
    
    
    func getSATResults(){
        SATResultsService.resultsOfSchool { results, error in
            guard error == nil else {
                return
            }
            
            guard let results = results else {
                return
            }
            self.satResult.value = results
        }
    }
    
    func getCurrentSchoolResults(withSchoolName:String? ,withSchoolID id: String) {
        let foundSchool = satResult.value.first(where: { result in
            guard let currentID = result.id else {return false}
            if currentID.uppercased() == id.uppercased() {
                return true
            }
            return false
        })
        if let name = foundSchool?.schoolName {
            schoolName.value = name
        }else{
            schoolName.value = withSchoolName
        }
        mathScore.value = foundSchool?.satMathAvgScore
        readingScore.value = foundSchool?.satCriticalReadingAvgScore
        writingScore.value = foundSchool?.satWritingAvgScore
    }
}
