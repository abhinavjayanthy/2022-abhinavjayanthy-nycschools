//
//  SATResultsService.swift
//  nycsschools
//
//  Created by Abhinav Jayanthy on 4/24/22.
//

import Foundation


class SATResultsService  {
    
    /*
     SAT results service to get the schools from the endpoint
     */
    static private let SATResultsURL = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!
    typealias SchoolResultsCompletion = ([SATResult]?,NYCOpenDataError?) -> ()
    
    static func resultsOfSchool(completion: @escaping SchoolResultsCompletion){
        let _ = NetworkingService.makeRequest(withURL: Self.SATResultsURL) { data, error in
            guard error == nil else {
                completion(nil,error)
                return
            }
            
            guard let data = data else{
                completion(nil,.invalidData)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let results = try decoder.decode([SATResult].self, from: data)
                completion(results,nil)
            } catch let error {
                print("The error is \(error)")
                completion(nil,.invalidData)
            }
        }
    }
}
