//
//  SchoolsService.swift
//  nycsschools
//
//  Created by Abhinav Jayanthy on 4/24/22.
//

import Foundation

enum NYCOpenDataError:Error{
    // Custom error codes 
    case invalidResponse
    case noData
    case failedRequest
    case invalidData
}

class SchoolsService  {
    
    /*
     School service to get the schools from the endpoint
     */
    static private let schoolsURL = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
    typealias SchoolsDataCompletion = ([School]?,NYCOpenDataError?) -> ()
    
    static func schoolsInNyc(completion: @escaping SchoolsDataCompletion){
       let _ =  NetworkingService.makeRequest(withURL: Self.schoolsURL) { data, error in
            guard error == nil else {
                completion(nil,error)
                return
            }
            
            guard let data = data else{
                completion(nil,.invalidData)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let schools = try decoder.decode([School].self, from: data)
                completion(schools,nil)
            } catch let error {
                print("The error is \(error)")
                completion(nil,.invalidData)
            }
        }
    }
}
